import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from 'react-router-dom';
import AppHouse from './Components/Entry/AppHouse'

ReactDOM.render(
    <BrowserRouter>
        <AppHouse/>
    </BrowserRouter>, 
document.getElementById('root'));
registerServiceWorker();
