import React, {Component} from 'react';
import Notifications from '../Dropdown/Notifications';
import AuthDropDown from '../Dropdown/AuthDropDown';


class Layout extends Component{
    render(){
        return(
            <div id="header">
                <header className="clearfix">
                    <div className="branding">
                        <a className="brand" href="/home"></a>
                        <a role="button" tabIndex={0} className="offcanvas-toggle visible-xs-inline">
                            <i className="fa fa-bars" />
                        </a>
                    </div>
                    <ul className="nav-left pull-left list-unstyled list-inline">
                        <li className="leftmenu-collapse">
                            <a role="button" tabIndex={0} className="collapse-leftmenu">
                            <i className="fa fa-outdent" />
                            </a>
                        </li>                
                    </ul>
                    <div className="search" id="main-search">
                        <input type="text" className="form-control underline-input" placeholder="Explore Falcon..." />
                    </div>
                    <ul className="nav-right pull-right list-inline">                                
                        <Notifications/>
                        <AuthDropDown/>
                    </ul>
                </header>
            </div>
        )
    }
}

export default Layout;