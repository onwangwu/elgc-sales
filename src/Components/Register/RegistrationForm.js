import React, { Component } from 'react';
import TextInput from '../FormInputs/TextInput';
import CheckBox from '../FormInputs/CheckBox';
import axios from 'axios';
import Logo from '../FormInputs/Logo';

class RegistrationForm extends Component{

  constructor(props){
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      agree: false,
      errorFirstName: "",
      errorLastName: "",
      errorEmail:"",
      errorPassword: "",
      errorAgree:""
    }
  }

  changeValue = (keyVal,value) => {
    let prop = {};
    prop[keyVal] = value;
    return this.setState(prop);
  }

  handleRegister(event){
    var baseUrl = 'http://localhost:3001/users/create';
    var payload = {
      "firstName": this.state.firstName,
      "lastName": this.state.lastName,
      "email": this.state.email,
      "password": this.state.password,
      "agree": this.state.agree
    };
    
    axios.post(baseUrl,payload)
    .then((response) => {
      this.state.agree === false?this.setState({errorAgree: "You must accept our terms and conditions"}):this.setState({errorAgree: ""})

      if(response.data.status === "validationError"){
        this.setState(
          {
            errorFirstName: response.data.validationErrors.firstName,
            errorLastName: response.data.validationErrors.lastName,
            errorEmail: response.data.validationErrors.email,
            errorPassword: response.data.validationErrors.password,
          }
        );
        console.log(this.state);
      }
      
    })
    .catch(function(error){
      console.log(error);
    });
  }


  render () {
      var style = {
        align: 'center',
        height: '180px'
      }
      return (       
        <div>
          <form className="form">
              <div className="header header-primary text-center">
                <h4>Register</h4>               
              </div>
              <Logo/>
              <div className="content">
                <TextInput placeholder="First Name" inputValue={this.state.firstName} keyVal="firstName" changeValue={this.changeValue} message={this.state.errorFirstName} />
                <TextInput placeholder="Last Name" inputValue={this.state.lastName} keyVal="lastName" changeValue={this.changeValue} message={this.state.errorLastName}/>
                <TextInput placeholder="Email" keyVal="email" inputValue={this.state.email} changeValue={this.changeValue} message={this.state.errorEmail}/>
                <TextInput placeholder="Password" type="password" inputValue={this.state.password} keyVal="password" changeValue={this.changeValue} message={this.state.errorPassword}/>
                <CheckBox labelText="I agree with the terms & Conditions" inputValue={this.state.agree} keyVal="agree" changeValue={this.changeValue} message={this.state.errorAgree}/>
              </div>
              <div className="footer text-center">
                <button className="btn btn-info btn-raised" type="button" onClick = {(event) => {event.preventDefault(); this.handleRegister(event)}}>Sign In</button>
              </div>
              <a href="/" className="btn btn-wd">Login Here</a>
            </form>

        </div>
      );
  }
}

export default RegistrationForm;



