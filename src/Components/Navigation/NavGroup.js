import React, {Component} from 'react';

class NavGroup extends Component{
    render(){
        return(
            <li> <a role="button" tabIndex={0}><i className="fa fa-envelope" /> <span>Mail Box</span> </a>
                <ul>
                    <li><a href="mail-inbox.html"><i className="fa fa-angle-right" /> Inbox <span className="label label-success">new</span></a></li>
                    <li><a href="mail-compose.html"><i className="fa fa-angle-right" /> Compose Mail</a></li>
                    <li><a href="mail-single.html"><i className="fa fa-angle-right" /> Single Mail</a></li>
                </ul>
            </li>
        )
    }
}

export default NavGroup;