import React, {Component} from 'react';
import DownSideBar from './DownSideBar';
import TopSideBar from './TopSideBar';

class Layout extends Component{
    render(){
        return(
            <div id="controls">
                <aside id="leftmenu">
                    <div id="leftmenu-wrap">
                        <div className="panel-group slim-scroll" role="tablist">
                            <TopSideBar/>
                            <DownSideBar/>
                        </div>
                    </div>
                </aside>
            </div>
        )
    }
}

export default Layout;