import React, {Component} from 'react';

class DownSideBar extends Component{
    render(){
        return(
            <div className="panel settings panel-default">
                <div className="panel-heading" role="tab">
                    <h4 className="panel-title"><a data-toggle="collapse" href="#leftmenuControls">General Settings <i className="fa fa-angle-up" /></a></h4>
                </div>
                <div id="leftmenuControls" className="panel-collapse collapse in" role="tabpanel">
                    <div className="panel-body">
                        <div className="form-group">
                            <div className="row">
                                <label className="col-xs-8">Switch ON</label>
                                <div className="col-xs-4 control-label">
                                    <div className="togglebutton">
                                        <label>
                                            <input type="checkbox" defaultChecked />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="row">
                                <label className="col-xs-8">Switch OFF</label>
                                <div className="col-xs-4 control-label">
                                    <div className="togglebutton">
                                        <label>
                                        <input type="checkbox" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="milestone-sidbar">
                    <div className="text-center-folded"> <span className="pull-right pull-none-folded">60%</span> <span className="hidden-folded">Milestone</span> </div>
                    <div className="progress progress-xxs m-t-sm dk">
                        <div className="progress-bar progress-bar-info" style={{width: '60%'}}> </div>
                    </div>
                    <div className="text-center-folded"> <span className="pull-right pull-none-folded">35%</span> <span className="hidden-folded">Release</span> </div>
                    <div className="progress progress-xxs m-t-sm dk">
                        <div className="progress-bar progress-bar-primary" style={{width: '35%'}}> </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DownSideBar;