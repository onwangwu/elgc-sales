import React, {Component} from 'react';
import SideBar from './SideBar';

class TopSideBar extends Component{
    render(){
        return(
            <div className="panel panel-default">						
                <div id="leftmenuNav" className="panel-collapse collapse in" role="tabpanel">
                    <div className="panel-body"> 
                        <SideBar/>
                    </div>
                </div>
            </div>
        )
    }
}
export default TopSideBar;