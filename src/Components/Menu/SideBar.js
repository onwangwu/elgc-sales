import React, {Component} from 'react';
import NavGroup from '../Navigation/NavGroup';
import NavLink from '../Navigation/NavLink';

class SideBar extends Component{
    render(){
        return(
            <ul id="navigation">
                <NavLink/>
                <NavGroup/>
                
            </ul>
        )
    }
}
export default SideBar;