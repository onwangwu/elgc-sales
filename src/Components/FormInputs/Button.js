import React, {Component} from 'react';

class SubmitButton extends Component{
    constructor(props){
        super(props);
        this.state = {
            inputValue: false
        };
    }

    render(){
        return(
            <div className="form-group">
                <button className="btn btn-info btn-block" type="button" onClick = {(event) => this.handleRegister(event)} >Sign up</button>
            </div>
        );
    }
    updateInputValue(evt) {
        this.setState({
          inputValue: evt.target.value
        });
      }
    
}

export default SubmitButton;