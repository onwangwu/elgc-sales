import React, {Component} from 'react';

class SocialSignUp extends Component{
    render(){
        return(
            <div className="social-line">
                <a className="btn btn-just-icon">
                <i className="fa fa-facebook-square" />
                </a>
                <a className="btn btn-just-icon">
                <i className="fa fa-twitter" />
                </a>
                <a className="btn btn-just-icon">
                <i className="fa fa-google-plus" />
                </a>
            </div>
        );
    }
}