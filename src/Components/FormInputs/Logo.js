import React, {Component} from 'react';

class Logo extends Component{
    render(){
        var style = {
            align: 'center',
            height: '180px'
          }
        return(
            <img src="/elgc_logo1.png" style={style} />
        );
    }
}
export default Logo;