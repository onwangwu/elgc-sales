import React, {Component} from 'react';
import ErrorMessage from '../Alerts/ErrorMessage';

class TextInput extends Component{

    render(){
        return(
            <div className="form-group">
                <input className="form-control underline-input" 
                type={this.props.type}  
                onChange={evt => this.props.changeValue(this.props.keyVal, evt.target.value)} 
                placeholder={this.props.placeholder}/>
                <ErrorMessage message={this.props.message} />
            </div>
        );
    }

    
}

export default TextInput;