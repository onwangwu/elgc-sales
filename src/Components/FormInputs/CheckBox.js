import React, {Component} from 'react';
import ErrorMessage from '../Alerts/ErrorMessage';
class CheckBox extends Component{

    render(){
        return(
            // <div className="checkbox">
            //     <label>
            //         <input type="checkbox" onChange={evt => this.props.changeValue(this.props.keyVal, !this.props.inputValue)}/>
            //         {this.props.labelText}
            //     </label>
            //     <ErrorMessage message={this.props.message} />
            // </div>
            <div className="togglebutton">
                
                <label>
                    <input type="checkbox" onChange={evt => this.props.changeValue(this.props.keyVal, !this.props.inputValue)}/>
                    {this.props.labelText}
                </label>
                
            </div>
    );
    

    }
    
}

export default CheckBox;