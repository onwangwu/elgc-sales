import React, {Component} from 'react';

class Layout extends Component{
    render(){
        return(
            <section id="content">
                <div className="page full-pagewidth-layout">
                    <div className="bg-light lter b-b wrapper-md mb-10">
                    <div className="row">
                        <div className="col-sm-6 col-xs-12">
                        <h1 className="font-thin h3 m-0">Boxed Layout</h1>
                        <small className="text-muted">Welcome to Falcon application</small>
                        </div>
                    </div>
                    </div>			
                    <p className="lead">This is the full-width layout template.</p>
                </div>
            </section>
        )
    }
}

export default Layout;