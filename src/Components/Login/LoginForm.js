import React, { Component } from 'react';
import axios from 'axios';
import TextInput from '../FormInputs/TextInput';
import CheckBox from '../FormInputs/CheckBox';
import Logo from '../FormInputs/Logo';
import env from '../../config/env';
import {Redirect} from 'react-router-dom';

class LoginForm extends Component{

    constructor(props){
      super(props)
      this.state = {
        email: "",
        password: "",
        errorEmail: "",
        errorPassword: "",
        agree: false,
        isAuthenticated: false 
      }
    }
    changeValue = (keyVal,value) => {
      let prop = {};
      prop[keyVal] = value;
      return this.setState(prop);
    }
    handleSignIn(event){
      var baseUrl = env.appPath+'users/login';
      var payload = {
        "email": this.state.email,
        "password": this.state.password,
        "agree": this.state.agree
      };
      axios.post(baseUrl,payload)
      .then((response) => {
        console.log(this.state)
        if(response.data.status === 'success'){
          this.setState({
            isAuthenticated: true
          });
          window.document.cookie = response.data
        }
        if(response.data.status === 'validationError'){
          this.setState({
            errorEmail: response.data.validationErrors.email,
            errorPassword: response.data.validationErrors.password,
          });
        } else if(response.data.status === 'userNotFound'){
          this.setState({
            errorEmail: response.data.message
          });
        } else if(response.data.status === 'passwordIncorrect'){
          this.setState({
            errorPassword: response.data.message
          });
        }
      }).catch((err) => {
        console.log(err);
      });
    }

    componentDidUpdate(){
      if(this.state.isAuthenticated === true){
        // this.props.history.push('/home')
        window.location.href = '/home';
      }
    }
    render () {

        return (
          <div>
            <form className="form">
              <div className="header header-primary text-center">
                <h4>Sign in</h4>
              </div>
              <Logo/>
              <div className="content">
                <TextInput placeholder="Email Address" inputValue={this.state.email} keyVal="email" changeValue={this.changeValue} message={this.state.errorEmail} />
                <TextInput placeholder="Password" inputValue={this.state.password} keyVal="password" changeValue={this.changeValue} message={this.state.errorPassword} />
                <CheckBox labelText="Remember Me" inputValue={this.state.agree} keyVal="agree" changeValue={this.changeValue} message={this.state.errorAgree}/>
              </div>
              <div className="footer text-center">
                <button className="btn btn-info btn-raised"  onClick = {(event) => {event.preventDefault(); this.handleSignIn(event)}}>Sign In</button>
              </div>
              <a href="/register" className="btn btn-wd">Register Here</a>
            </form>
          </div>
        );
     

        
    }
}

export default LoginForm;


