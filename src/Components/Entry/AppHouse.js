import React, {Component} from 'react';
import {Route,Switch} from 'react-router-dom';
import AppLayout from '../AppLayout/AppLayout' ;
import Entry from '../Entry/EntryComponent';

class AppHouse extends Component{
    render(){
        return(
            <Switch>
                <Route path='/home'  component={AppLayout} />
                <Entry/>
            </Switch>
        )
    }
}
export default AppHouse;