import React, { Component } from 'react';
import RegistrationForm from '../Register/RegistrationForm';
import LoginForm from '../Login/LoginForm';
import {Route, Switch} from 'react-router-dom';
import '../../assets/css/style.css';


class Entry extends Component{
    render(){

        return (
          <div id="falcon" className="authentication" >            
            <div className="wrapper">
              <div className="header header-filter osita">
                <div className="container">
                  <div className="row">
                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
                      <div className="card card-signup">                      
                            <Switch>
                                <Route path='/register'component={RegistrationForm}/>
                                <Route exact={true} path='/' component={LoginForm}/>
                            </Switch>
                      </div>
                    </div>
                  </div>
                </div>
                <footer className="footer mt-20">
                  <div className="container">
                    <div className="col-lg-12 text-center">
                      <div className="copyright text-white mt-20"> Enugu Lifestyle &amp; Golf City © 2018
                      </div>
                    </div>
                  </div>
                </footer>
              </div>
            </div>
          </div>
           
        );
    }
}

export default Entry;