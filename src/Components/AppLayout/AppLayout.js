import React, {Component} from 'react';
import MenuLayout from '../Menu/Layout';
import HeaderLayout from '../Header/Layout';
import ContentLayout from '../Content/Layout';



class AppLayout extends Component{

    

    render(){
        var style = {
            opacity: 1
        }
        return(
            <div id="wrap" style={style} className="animsition"> 

                <HeaderLayout/>
                <MenuLayout/>
                <ContentLayout/>

            </div>

        );
    }
}

export default AppLayout;