import React, {Component} from 'react';

class Notifications extends Component{
    render(){
        return(
            <li className="dropdown notifications">
                <a href="" className="dropdown-toggle" data-toggle="dropdown">
                    <i className="fa fa-bell" />
                    <div className="notify">
                        <span className="heartbit" />
                        <span className="point" />
                    </div>
                </a>
                <div className="dropdown-menu pull-right panel panel-default">
                    <ul className="list-group">
                        <li className="list-group-item">
                            <a role="button" tabIndex={0} className="media">
                                <span className="pull-left media-object media-icon">
                                <i className="fa fa-ban" />
                                </span>
                                <div className="media-body">
                                <span className="block">User Lucas cancelled account</span>
                                <small className="text-muted">12 minutes ago</small>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <div className="panel-footer">
                        <a role="button" tabIndex={0}>Show all notifications
                            <i className="fa fa-angle-right pull-right" />
                        </a>
                    </div>
                </div>
            </li>
        );
    }
}

export default Notifications;