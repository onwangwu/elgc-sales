import React, {Component} from 'react';

class AuthDropDown extends Component{
    render(){
        return(
            <li className="dropdown nav-profile">
                <a href="" className="dropdown-toggle" data-toggle="dropdown">
                <img src="assets/images/profile-photo.jpg"  className="0 size-30x30" /> </a>
                <ul className="dropdown-menu pull-right" role="menu">
                    <li>
                        <div className="user-info">
                        <div className="user-name">Alexander</div>
                        <div className="user-position online">Available</div>
                        </div>
                    </li>
                    <li>
                        <a href="profile.html" role="button" tabIndex={0}>
                        <span className="label label-success pull-right">80%</span>
                        <i className="fa fa-user" />Profile</a>
                    </li>
                    <li>
                        <a role="button" tabIndex={0}>
                        <span className="label label-info pull-right">new</span>
                        <i className="fa fa-check" />Tasks</a>
                    </li>
                    <li>
                        <a role="button" tabIndex={0}>
                        <i className="fa fa-cog" />Settings</a>
                    </li>
                    <li className="divider" />
                    <li>
                        <a href="locked.html" role="button" tabIndex={0}>
                        <i className="fa fa-lock" />Lock</a>
                    </li>
                    <li>
                        <a href="login.html" role="button" tabIndex={0}>
                        <i className="fa fa-sign-out" />Logout</a>
                    </li>
                </ul>
            </li>
        )
    }
}

export default AuthDropDown;