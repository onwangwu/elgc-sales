import React, {Component} from 'react';

class ErrorMessage extends Component{

    render(){
        const divStyle = {
            margin: '1px',
            backgroundColor: '#feebec',
            borderRadius: '2px'
          };
        return(
            <p style={divStyle} >
                {this.props.message} 
            </p>
        );
    }
}

export default ErrorMessage;