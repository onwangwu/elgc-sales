var express = require('express');
var router = express.Router();
var controller = require('../app/Controllers/CommunityController');

/* GET users listing. */
router.post('/create', controller.addCommunity);
router.get('/',controller.getAllCommunities);
router.put('/update/:id',controller.updateCommunity);
router.delete('/delete/:id',controller.deleteCommunityById);

module.exports = router;