var express = require('express');
var router = express.Router();
var controller = require('../app/Controllers/AgentController');

/* GET users listing. */
router.post('/create', controller.addAgent);
router.get('/',controller.getAllAgents);
router.put('/update/:id',controller.updateAgent);
router.delete('/delete/:id',controller.deleteAgentById);

module.exports = router;