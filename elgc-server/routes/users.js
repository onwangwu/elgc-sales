var express = require('express');
var router = express.Router();
var UserController = require('../app/Controllers/UserController');

/* GET users listing. */
router.get('/', UserController.getAllUsers);
router.post('/create', UserController.addUser);
router.post('/login',UserController.loginUser);
router.put('/update/:id', UserController.updateUser);
router.delete('/delete/:id', UserController.deleteUserById);

module.exports = router;
