var express = require('express');
var router = express.Router();
var controller = require('../app/Controllers/PlotController');

/* GET users listing. */
router.post('/create', controller.addPlot);
router.get('/',controller.getAllPlots);
router.put('/update/:id',controller.updatePlot);
router.delete('/delete/:id',controller.deletePlotById);

module.exports = router;