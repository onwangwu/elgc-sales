var express = require('express');
var router = express.Router();
var controller = require('../app/Controllers/PaymentMethodController');

/* GET users listing. */
router.post('/create', controller.addPaymentMethod);
router.get('/',controller.getAllPaymentMethods);
router.put('/update/:id',controller.updatePaymentMethod);
router.delete('/delete/:id',controller.deletePaymentMethodById);

module.exports = router;