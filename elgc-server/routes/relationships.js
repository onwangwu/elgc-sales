var express = require('express');
var router = express.Router();
var controller = require('../app/Controllers/RelationshipController');

/* GET users listing. */
router.post('/create', controller.addRelationship);
router.get('/',controller.getAllRelationships);
router.put('/update/:id',controller.updateRelationship);
router.delete('/delete/:id',controller.deleteRelationshipById);

module.exports = router;