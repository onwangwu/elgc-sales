var express = require('express');
var router = express.Router();
var controller = require('../app/Controllers/PaymentController');

/* GET users listing. */
router.post('/create', controller.addPayment);
router.get('/',controller.getAllPayments);
router.put('/update/:id',controller.updatePayment);
router.delete('/delete/:id',controller.deletePaymentById);

module.exports = router;