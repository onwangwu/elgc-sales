var express = require('express');
var router = express.Router();
var controller = require('../app/Controllers/ClientController');

/* GET users listing. */
router.post('/create', controller.addClient);
router.get('/',controller.getAllClients);
router.put('/update/:id',controller.updateClient);
router.delete('/delete/:id',controller.deleteClientById);

module.exports = router;