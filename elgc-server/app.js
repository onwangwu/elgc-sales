var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var agentRouter = require('./routes/agents');
var clientRouter = require('./routes/clients');
var paymentRouter = require('./routes/payments');
var paymentMethodRouter = require('./routes/paymentMethods');
var plotRouter = require('./routes/plots');
var relationshipRouter = require('./routes/relationships');
var usersRouter = require('./routes/users');
var communityRouter = require('./routes/community');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/elgc', { useNewUrlParser: true });
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/communities', communityRouter);
app.use('/agents', agentRouter);
app.use('/clients', clientRouter);
app.use('/paymentMethods', paymentMethodRouter);
app.use('payments',paymentRouter);
app.use('/plots',plotRouter);
app.use('/relationships',relationshipRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
