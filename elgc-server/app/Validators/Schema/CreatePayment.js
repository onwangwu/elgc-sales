const joi = require('joi');

module.exports = joi.object().keys({
    amount: joi.string().required(),
    relationship: joi.string().required(),
    paymentMethod: joi.string().required()
});