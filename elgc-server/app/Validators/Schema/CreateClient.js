const joi = require('joi');

module.exports = joi.object().keys({
    firstName: joi.string().required(),
    middleName: joi.string().required(),
    lastName: joi.string().required(),
    email: joi.string().email().required(),
    phone: joi.number().required(),
    address: joi.string().required()
});