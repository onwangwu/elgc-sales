const joi = require('joi');

module.exports = joi.object().keys({
    client: joi.string().required(),
    plot: joi.string().required(),
    duration: joi.number().required(),
    installments: joi.number().required(),
    pricePerInstallment: joi.number().required(),
    interval: joi.number().required(),
    agent: joi.string().required()
});