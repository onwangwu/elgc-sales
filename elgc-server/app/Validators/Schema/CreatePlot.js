var joi = require('joi');

module.exports = joi.object().keys({
    plotNumber: joi.number().required(),
    plotSize: joi.number().required(),
    community: joi.string().required(),
    plotPrice: joi.number().required()
});