var repository = require('../Repositories/RelationshipRepository');
var BaseService = require('../Services/BaseService');
var schema = require('../Validators/Schema/CreateRelationship');


function RelationshipService(){

}

RelationshipService.prototype = BaseService(repository, schema);

module.exports = new RelationshipService();