var repository = require('../Repositories/CommunityRepository');
var schema = require('../Validators/Schema/CreateCommunity');
var BaseService = require('../Services/BaseService');

function CommunityService(){

}

CommunityService.prototype = BaseService(repository,schema);

module.exports = new CommunityService();