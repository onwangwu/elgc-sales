var repository = require('../Repositories/AgentRepository');
var schema = require('../Validators/Schema/CreateAgent');
var BaseService = require('../Services/BaseService');

function AgentService(){

}

AgentService.prototype = BaseService(repository,schema);

module.exports = new AgentService();