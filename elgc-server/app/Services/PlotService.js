var repository = require('../Repositories/PlotRepository');
var BaseService = require('../Services/BaseService');
var schema = require('../Validators/Schema/CreatePlot');

function PlotService(){

}

PlotService.prototype = BaseService(repository, schema);

module.exports = new PlotService();