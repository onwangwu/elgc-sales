var repository = require('../Repositories/UserRepository');
var schema = require('../Validators/Schema/CreateUser');
var BaseService = require('../Services/BaseService');
var jwt = require('jsonwebtoken');
var jwtConfig = require('../config/jwt');

let UserService = new BaseService(repository,schema);

UserService.login = function(req,res,options){
    var loginSchema = require('../Validators/Schema/UserLogin');
    var validationErrors = this.validator.isValid(req,res,loginSchema,options);
    if(validationErrors !== null){
        return res.json({status:'validationError', validationErrors});
    } 
    this.repository.getOne({email: options.email},function(err,data){
        if(err) res.json(err);
        if(!data){
            res.json({status: 'userNotFound', message: 'Oops! We do not know you. Please enter a valid email'})
        } else if(data){
            if(data.password !== options.password){
                res.json({status: 'passwordIncorrect', message:'Oops! Seems you entered a wrong password.'});
            } else{
                const payload = {
                    user: data.email,
                    id: data._id
                };
                var token = jwt.sign(payload,jwtConfig.secret,{
                    expiresIn: 1440
                });
                res.json({
                    status: 'success',
                    message: 'User logged in successfully.',
                    token: token
                });
            }
        }
        
    });
}

UserService.register = function(req,res,options){
    var validationErrors = this.validator.isValid(req,res,this.schema,options);
    if(validationErrors !== null){
        return res.json({status: 'validationError', validationErrors});
    }
    this.repository.add(options,(err,user) => {
        if(err) res.json(err)
        //this.login(req,res,{email: options.email,password: options.password});
        
    });
}

module.exports = UserService;


