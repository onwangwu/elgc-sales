var repository = require('../Repositories/PaymentRepository');
var schema = require('../Validators/Schema/CreatePayment');
var BaseService = require('../Services/BaseService');

function PaymentService(){

}

PaymentService.prototype = BaseService(repository,schema);

module.exports = new PaymentService();
