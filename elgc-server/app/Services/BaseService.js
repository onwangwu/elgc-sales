function BaseService(repository,schema){
    if(!repository) throw new Error('A valid repository must be provided')
    this.repository = repository;
    this.validator = require('../Validators/Validator');
    this.schema = schema;
    
}

BaseService.prototype.create = function(req,res,options) {
    // calling validator
    var validationErrors = this.validator.isValid(req,res,this.schema,options);

    if(validationErrors !== null){
        return res.json({status:'validationError', validationErrors});
    }   
    this.repository.add(options, function(err, data){
        if(err) res.json({status: 'error', data: err});
        res.json({status: 'success', data: data});
    });
} 

BaseService.prototype.fetch = function(req,res,options){
    this.repository.getAll(options, function(err, data){
        if(err) res.json({ status: 'error', data: err });
        res.json({status: 'success', data: data});
    });
}

BaseService.prototype.update = function(req,res,id,options){
    var valid = this.validator.isValid(req,res,this.schema,options);

    if(valid !== null){
        return res.json(valid);
    }   
    this.repository.update(id,options,function(err, data){
        if(err) res.json({status: 'error', data: err});
        res.json({status: 'success', data: data});
    });
}

BaseService.prototype.destroy = function(req,res,options){
    this.repository.delete(options, function(err, data){
        if(err) res.json({status: 'error', data: err});
        res.json({status: 'success', data: data});
    });
}


module.exports = function(repository,schema){
    return new BaseService(repository, schema);
}

