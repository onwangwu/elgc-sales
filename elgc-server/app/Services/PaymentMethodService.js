var repository = require('../Repositories/PaymentMethodRepository');
var schema = require('../Validators/Schema/CreatePaymentMethod');
var BaseService = require('../Services/BaseService');

function PaymentMethodService(){

}

PaymentMethodService.prototype = BaseService(repository, schema);

module.exports = new PaymentMethodService();

