var service = require('../Services/PaymentService');



exports.addPayment = function(req,res){
    var options = {
        amount: req.body.amount,
        relationship: req.body.relationship,
        paymentMethod: req.body.paymentMethod

    };
    return service.create(req,res,options);
}

exports.getAllPayments = function(req,res){
    var options = {};
    return service.fetch(req,res,options);
}

exports.updatePayment = function(req,res){
    var id = {
        _id: req.params.id
    };
    var options = {
        amount: req.body.amount,
        relationship: req.body.relationship,
        paymentMethod: req.body.paymentMethod

    };
    return service.update(req,res,id,options);
}

exports.deletePaymentById = function(req,res){
    var options = {
        _id: req.params.id
    }
    return service.destroy(req,res,options);
}