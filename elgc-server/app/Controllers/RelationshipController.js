var service = require('../Services/RelationshipService');



exports.addRelationship = function(req,res){
    var options = {
        client: req.body.client,
        plot: req.body.plot,
        duration: req.body.duration,
        installments: req.body.installments,
        pricePerInstallment: req.body.pricePerInstallment,
        interval: req.body.interval,
        agent: req.body.agent,

    };
    return service.create(req,res, options);
}

exports.getAllRelationships = function(req,res){
    var options = {};
    return service.fetch(req,res,options);
}

exports.updateRelationship = function(req,res){
    var id = {
        _id: req.params.id
    };
    var options = {
        client: req.body.client,
        plot: req.body.plot,
        duration: req.body.duration,
        installments: req.body.installments,
        pricePerInstallment: req.body.pricePerInstallment,
        interval: req.body.interval,
        agent: req.body.agent,

    };
    return service.update(req,res,id,options);
}

exports.deleteRelationshipById = function(req,res){
    var options = {
        _id: req.params.id
    }
    return service.destroy(req,res,options);
}