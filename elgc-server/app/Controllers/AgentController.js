var service = require('../Services/AgentService');



exports.addAgent = function(req,res){
    var options = {
        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone,
        address: req.body.address,

    };
    return service.create(req,res, options);
}

exports.getAllAgents = function(req,res){
    var options = {};
    return service.fetch(req,res,options);
}

exports.updateAgent = function(req,res){
    var id = {
        _id: req.params.id
    };
    var options = {
        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone,
        address: req.body.address,

    };
    return service.update(req,res,id,options);
}

exports.deleteAgentById = function(req,res){
    var options = {
        _id: req.params.id
    }
    return service.destroy(req,res,options);
}