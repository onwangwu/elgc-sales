var service = require('../Services/CommunityService');



exports.addCommunity = function(req,res){
    var options = {
        name: req.body.name,
    };
    return service.create(req,res, options);
}

exports.getAllCommunities = function(req,res){
    var options = {};
    return service.fetch(req,res,options);
}

exports.updateCommunity = function(req,res){
    var id = {
        _id: req.params.id
    };
    var options = {
        name: req.body.name
    };
    return service.update(req,res,id,options);
}

exports.deleteCommunityById = function(req,res){
    var options = {
        _id: req.params.id
    }
    return service.destroy(req,res,options);
}