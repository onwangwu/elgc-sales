var service = require('../Services/UserService');



exports.addUser = function(req,res){
    var options = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,

    };
    return service.register(req,res, options);
}

exports.getAllUsers = function(req,res){
    var options = {};
    return service.fetch(req,res,options);
}

exports.updateUser = function(req,res){
    var id = {
        _id: req.params.id
    };
    var options = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,

    };
    return service.update(req,res,id,options);
}

exports.deleteUserById = function(req,res){
    var options = {
        _id: req.params.id
    }
    return service.destroy(req,res,options);
}

exports.loginUser = function(req,res){
    var options = {
        email: req.body.email,
        password: req.body.password,
        //agree: req.body.agree
    }
    return service.login(req,res,options);
}