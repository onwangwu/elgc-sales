var service = require('../Services/PaymentMethodService');



exports.addPaymentMethod = function(req,res){
    var options = {
        name: req.body.amount,   
    };
    return service.create(req,res, options);
}

exports.getAllPaymentMethods = function(req,res){
    var options = {};
    return service.fetch(req,res,options);
}

exports.updatePaymentMethod = function(req,res){
    var id = {
        _id: req.params.id
    };
    var options = {
        name: req.body.amount,
    };
    return service.update(req,res,id,options);
}

exports.deletePaymentMethodById = function(req,res){
    var options = {
        _id: req.params.id
    }
    return service.destroy(req,res,options);
}