var service = require('../Services/PlotService');



exports.addPlot = function(req,res){
    var options = {
        plotNumber: req.body.plotNumber,
        plotSize: req.body.plotSize,
        community: req.body.community,
        plotPrice: req.body.plotPrice,

    };
    return service.create(req,res, options);
}

exports.getAllPlots = function(req,res){
    var options = {};
    return service.fetch(req,res,options);
}

exports.updatePlot = function(req,res){
    var id = {
        _id: req.params.id
    };
    var options = {
        plotNumber: req.body.plotNumber,
        plotSize: req.body.plotSize,
        community: req.body.community,
        plotPrice: req.body.plotPrice,

    };
    return service.update(req,res,id,options);
}

exports.deletePlotById = function(req,res){
    var options = {
        _id: req.params.id
    }
    return service.destroy(req,res,options);
}