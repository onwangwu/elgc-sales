var service = require('../Services/ClientService');



exports.addClient = function(req,res){
    var options = {
        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone,
        address: req.body.address,

    };
    return service.create(req,res, options);
}

exports.getAllClients = function(req,res){
    var options = {};
    return service.fetch(req,res,options);
}

exports.updateClient = function(req,res){
    var id = {
        _id: req.params.id
    };
    var options = {
        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone,
        address: req.body.address,

    };
    return service.update(req,res,id,options);
}

exports.deleteClientById = function(req,res){
    var options = {
        _id: req.params.id
    }
    return service.destroy(req,res,options);
}