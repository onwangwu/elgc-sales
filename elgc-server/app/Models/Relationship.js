var mongoose = require('mongoose');

var RelationshipSchema = mongoose.Schema({
    client: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    },
    plot: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Plot'
    },
    duration: {
        type: Number
    },
    installments: {
        type: Number
    },
    pricePerInstallment: {
        type: Number
    },
    interval: {
        type: Number
    },
    agent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Agent'
    },
    payments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payment'
    }]
    
});

module.exports = mongoose.model('Relationship', RelationshipSchema);