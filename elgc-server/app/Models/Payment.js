var mongoose = require('mongoose');

var PaymentSchema = mongoose.Schema({
    amount: {
        type: Number
    },
    relationship: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Relationship'
    },
    paymentMethod: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PaymentMehtod'
    }

});

module.exports = mongoose.model('Payment', PaymentSchema);