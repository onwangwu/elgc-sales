var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    firstName:{
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String,
        unique: true,
    },
    password: String,

});

module.exports = mongoose.model('User', UserSchema);