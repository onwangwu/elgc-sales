var mongoose = require('mongoose');

var CommunitySchema = mongoose.Schema({
    name: {
        type: String
    },
    plots: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Plot'
    }]
});

module.exports = mongoose.model('Community',CommunitySchema);