var mongoose = require('mongoose');

var PlotSchema = mongoose.Schema({
    plotNumber: {
        type: Number,
        unique: true
    },
    plotSize: {
        type: Number,
    },
    community: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Community'
    },
    plotPrice: {
        type: Number
    }    
});

module.exports = mongoose.model('Plot', PlotSchema);