var mongoose = require('mongoose');

var ClientSchema = mongoose.Schema({
    firstName:{
        type: String
    },
    middleName: {
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    phone: {
        type: Number,
        unique: true
    },
    address: {
        type: String
    },
    relationships: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Relationship'
    }]
});

module.exports = mongoose.model('Client', ClientSchema);