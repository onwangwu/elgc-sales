var mongoose = require('mongoose');

var PaymentMethodSchema = mongoose.Schema({
    name: {
        type: String
    },
    payments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payment'
    }]
});

module.exports = mongoose.model('PaymentMethod',PaymentMethodSchema);